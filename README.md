# Sending files #

In deze README.md staat de code voor het onderzoek 'Bestanden versturen'.
Dit onderzoek is gedaan om te kijken hoe de performance van een verstuurd bestand verbeterd kan worden, dus hoe bestanden op een veilige en efficiënte manier tussen de ene cliënt naar de andere cliënt(s) verstuurd kunnen worden.

### Wat kan je in de sourcecode vinden? ###

* Bestandscompressie, door middel van de 'lossy'-techniek  
* Chunken van een (gecompressed) bestand
* Encrypten en decrypten van een chunk (AES-256-CBC-encryption)
* Samenvoegen van de chunks in een gemerged bestand

### Testbestanden ###

In de resources-map (src -> main -> resources) zijn meerdere testbestanden (video's, foto's en bin-files) beschikbaar om de sourcecode te testen.

### Vragen? ###

* Neem contact op met de groep 'Bestanden versturen' voor fouten en onduidelijkheden :)

