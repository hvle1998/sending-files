package nl.han.asd.asdproject;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import java.io.*;
import java.nio.charset.StandardCharsets;

public class AESEncryption {
    private String initVector = "encryptionIntVec";
    private IvParameterSpec iv;
    private Cipher cipher;

    public AESEncryption(){
        iv = new IvParameterSpec(initVector.getBytes(StandardCharsets.UTF_8));
    }

    public void encrypt(File chunkedFile, byte[] contentOfChunkedFile, SecretKey secretKey){
        try {
            cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey,iv);

            byte[] encrypted = cipher.doFinal(contentOfChunkedFile);

            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(chunkedFile));
            bos.write(encrypted);
            bos.flush();
            bos.close();
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

    public byte[] decrypt( byte[] encryptedContent, SecretKey secretKey){
        try {
            cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, secretKey,iv);

            return cipher.doFinal(encryptedContent);
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
