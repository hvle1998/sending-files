package nl.han.asd.asdproject;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class FileChunking {

    private static String recipe = "";

    //File chunking
    private static List<File> fileList = new ArrayList<File>();

    //AES encryption
    private static final AESEncryption aes = new AESEncryption();
    private static List<SecretKey> secretKeys = new ArrayList<>();

    public FileChunking(){

    }

    public static String getRecipe(){
        return recipe;
    }

    public static void splitFile(File f) throws IOException {
        int sizeOfFiles = 1024 * 1024; // +-1MB
        byte[] buffer = new byte[sizeOfFiles];

        //Input file as stream and split up/save in multiple files,
        //add those files to a List of Files
        try (FileInputStream fis = new FileInputStream(f);
             BufferedInputStream bis = new BufferedInputStream(fis)) {

            File newFile = null;
            int bytesAmount = 0;
            int i = 0;
            while ((bytesAmount = bis.read(buffer)) > 0) {
                //write each chunk of data into separate file with different number in name
                String filePartName = UUID.randomUUID().toString();
                newFile = new File(f.getParent(), filePartName);
                try (FileOutputStream out = new FileOutputStream(newFile)) {
                    out.write(buffer, 0, bytesAmount);
                }
                encryptChunk(newFile);
                fileList.add(newFile);
                addToRecipe(newFile);
                i++;
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public static void addToRecipe(File file) {
        recipe += file.getName() + " ";
    }

    public static void getFilesFromRecipe(String recipe, File into) throws IOException {
        String[] fileNames = recipe.split("\\s+");
        List<File> files = new ArrayList<>();
        int i = 0;
        for (String fileName : fileNames){
            File chunk = new File("src/main/resources/" + fileName);
            decryptFile(chunk,secretKeys.get(i));
            files.add(chunk);
            i++;
        }
        mergeFiles(files, into);
    }

    public static void mergeFiles(List<File> files, File into)
            throws IOException {
        try (FileOutputStream fos = new FileOutputStream(into);
             BufferedOutputStream mergingStream = new BufferedOutputStream(fos)) {
            for (File f : files) {
                Files.copy(f.toPath(), mergingStream);
            }
        }
    }

    //Testen van het versturen van bestanden
    //S1: Opdelen -> Encrypten
    //S2: Encrypten -> Opdelen
    public static void encryptChunk(File chunkToEncrypt) throws IOException, NoSuchAlgorithmException {
        KeyGenerator generator = KeyGenerator.getInstance("AES");
        byte [] content = Files.readAllBytes(Paths.get(chunkToEncrypt.getAbsolutePath()));
        try{
            SecretKey secretKey = generator.generateKey();
            secretKeys.add(secretKey);

            aes.encrypt(chunkToEncrypt, content, secretKey);
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

    public static void decryptFile(File chunk, SecretKey secretKey) throws IOException {
        byte [] content = Files.readAllBytes(Paths.get(chunk.getAbsolutePath()));
        try{
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(chunk));
            bos.write(aes.decrypt(content, secretKey));
            bos.flush();
            bos.close();
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }
}
