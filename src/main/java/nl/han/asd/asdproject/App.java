package nl.han.asd.asdproject;

import java.io.*;
import java.util.*;

public class App {

    //Testfile
    private static String testFile = System.getProperty("user.dir") + "/src/main/resources/nature.jpg";

    //File compression
    private static String outputFile = "src/main/resources/compressed_nature.jpg";
    private static String format = "JPG";

    //File chunking
    private static File original;
    private static String recipe = "";
    private static List<File> fileList = new ArrayList<File>();
    private static File newFile;

    //Merged file
    private static String mergedFile = "newNature.jpg";

    public static void main(String[] args) throws IOException {
        original = new File(outputFile);
        newFile = new File(mergedFile);
        FileCompression.compressImage(testFile, outputFile, format);
        FileChunking.splitFile(original);
        FileChunking.getFilesFromRecipe(FileChunking.getRecipe(), newFile);
    }
}

